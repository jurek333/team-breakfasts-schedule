Dużo ostatnio javascriptu dookoła. Ciekawe o co tyle szumu? Może przydałoby się coś w tym napisać? Jakieś szybkie i proste API w **nodejs**? Pobieżne zerknięcie co wujek Google mówi na ten temat. Zainteresował mnie framework: **koa**. 
Do tego przydałaby się jeszcze jakaś ciekawa baza danych. To może **RethinkDB**. Co tu strzępić ozory. Do dzieła!
#####Instalacja DB
Tak, mam **Dockera** i nie zawacham się go użyć:

``docker run --name koadb -d -P rethinkdb``

To nam odpali kontener z zainstalowaną i działającą już rethinkdb. W procesie kodowania nie zależy mi na danych, które będę zapisywał do DB. Godzę się z faktem, że usuwając kontener stracę je. Dlatego nie tworzę w kontenerze woluminów (opcja **-v**). Pozostałe parametry: 

* **-d** (detached) zapewnia, że kontener po odpaleniu nie wpakuje się nam na konsolę, a ładnie i grzecznie zwróci ją nam. Sam zaś cichutko, w kącie, będzie kontynuował działanie.
* **-P** automatycznie mapuje udostępnione (ujawnione - EXPOSED) porty kontenera do dostępnych portów na naszej maszynie. Zamiast tego parametru można stosować *-p [localhost_port]:[container_port]* do określenia który port kontenera mapujemy na który port naszej maszyny.

Kiedy DB nie będzie nam już potrzebne wystarczy, że usuniemy kontener:

``docker rm koadb``

a nawet obraz:

``docker rmi rethinkdb``

I na komputerze ponownie zapanuje porządek. Brak śladów po jakichkolwiek bazach.
#####Zabieramy się za koa
*Node* 0.12.x zainstaloway jest już na maszynie. 
Tworzymy katalog nowego projektu, inicjujemy *npm* i instalujemy *koa* wraz z middleware od routingu *koa-route*. Instalujemy bibliotekę do współpracy z *rethinkdb* i implementacje puli połączeń do tej DB. No i nie zapomnajmy o przydatnym zawsze *lodash’u* (lub z czego Ty chcesz skożystać).

    mkdir koa-lab
    cd koa-lab
    npm init
    ... // odpowiadamy na pytania kreatora
    npm install koa koa-route --sav
    npm install rethinkdb generic-pool ti-rethinkdb-pool --save
    npm install lodash --save 
Teraz czas na kod *Hello world*.

    // [app.js]
    var koa = require("koa");
    var route = require("koa-route");
    var app = koa();
    app.use(route.get("/", function* () {
        this.body = "Hello world";
        this.status = 200;
    }));
    app.listen(9000);
Odpalenie aplikacji;

``node --harmony app.js``

I już rzuca się w oczy brak informacji: “czy to już? czy to działa?”.
Dopiszmy sobie na końcu pliku:

``console.log("HelloWorld nasłuchuje na porcie 9000");``

Od razu lepiej:

    jurek@Tensor:~/Projects/koa-lab$ node --harmony app.js
    HelloWorld nasłuchuje na porcie 9000
Koa silnie wykorzystuje generatory: ``function* (){...}``. Stąd ``--harmony`` wymagane do startu aplikacji. Zapomnisz - to zobaczysz:

    jurek@Tensor:~/Projects/koa-lab$ node app.js
    /home/jurek/Projects/koa-lab/app.js:4
    app.use(route.get(“/”, function* () {
                                   ^
            SyntaxError: Unexpected token *
            at exports.runInThisContext (vm.js:73:16)
            at Module._compile (module.js:443:25)
            at Object.Module._extensions..js (module.js:478:10)
            at Module.load (module.js:355:32)
            at Function.Module._load (module.js:310:12)
            at Function.Module.runMain (module.js:501:10)
            at startup (node.js:129:16)
            at node.js:814:3
Ale do brzegu. Pobawmy się dalej tym frameworkiem. Nie jest on jedyną dużą wszystkomającą biblioteką. Silnie opiera się na tak zwanych *middleware*. Spróbujmy zatem zrealizować coś w tym. Powiedzmy dodawanie do naglówka czasu przetwarzania zapytania i logowanie przychodzących zapytań.
Zacznijmy od czasu:

    var koa = require("koa");
    var route = require("koa-route");
    var app = koa();

    function* requestTime(next) {
        var start = new Date();
        yield next;
        var time = new Date() - start;
        this.set("X-Response-Time",time+"ms");
    }
    app.use(requestTime);

    app.use(route.get(“/”, function* () {
        this.body = "Hello world";
        this.status = 200;
    }));

    app.listen(9000);
    console.log("HelloWorld nasłuchuje na porcie 9000");
    console.log("Użyj Ctrl-C by zakończyć działanie.");
I oto dostajemy dodatkowy nagłówek *X-Response-Time* w odpowiedziach. To jeszcze logujmy na konsolę przychodzące żądania:

    [...]
    function* logRequest(next) {
        var dateTimeFormat = "YYYY-MM-DD hh:mm:ss";
        var logMsg = '['
            + moment().format(dateTimeFormat)
            +"] " + this.method 
            +", url: " + this.url;
        console.log(logMsg);
        yield next;
    }
    app.use(logRequest);

    app.listen(9000);
    [...]

Prawda, że ciekawie to wygląda. Uwagę zwraca ``yield next;``. To właśnie magia generatorów i przypomina w działaniu *await* z C#. Klasyczną asynchroniczność w js realizowało się poprzez funkcje callback. Tutaj z generatami wystarczy wywołać yield, wykonanie programu po zakończeniu operacji na które czekamy wróci do wykonywania dalszej części kodu. To na tyle ciekawy temat, że wart jest osobnego wpisu.

Czas się trochę zorganizować. Stworzyłem sobie podkatalog dla kodu API: ``koa-app/server-app`` i przeniosłem do niego plik *app.js*.

    ~/Projects/koa-lab/
        ▸ node_modules/
        ▾ server-app/
            app.js
        package.json
W package.json dodałem skrypt *start* uruchamiający aplikację:

    [...]
    "scripts": {
        "start": "node --harmony server-app/app.js",
        "test": "echo \"Error: no test specified\" "
    },
    [...]
Teraz wystarczy ``npm start`` zamiast ``node --harmony server-app/app.js``.

Zabierzmy się wreszcie za bazę. Na początek chciałbym mieć nazwy tabel i samej bazy w jednym miejscu, aby nie trzymać ich w prawie każdym pliku w projekcie. Wymyśliłem sobie, że utworzę plik *db/schema.js*, który będzie trzymał nazwy tabel i samej bazy danych również.

    // [db/schema.js]
    module.exports = {
        name: "breakfasts",
        tables: {
            users: "users",
            breakfasts: "breakfast",
            archive: "archive"
        }
    };

Teraz przydałby się kawałek kodu, który utworzy odpowiednią bazę i tabele jeśli jeszcze nie istnieją.

    // [db/initDb.js]
    module.exports = function (rethinkPool) {
        var r = rethinkPool;
        var db = require("./schema.js");
        var _ = require("lodash");

        var initTable = function() {
            return r.pool.run(r.db(db.name)
                .tableList()).then(function(tables){
                    _.each(tables , function(tableName){
                        console.log("existing table -> "+tableName);
                    });
                    _.each(db.tables, function(val, key){
                        if(!_.include(tables, val)) {
                            r.pool
                                .run(r.db(db.name).tableCreate(val))
                                .then(function(){
                                    console.log("table "+val+" created");
                                });
                        }
                });
            });
        }
        return {
            initialize: function () {
                return r.pool
                    .run(r.dbList())
                    .then(function(dbs){
                        _.each(dbs, function(dbName){
                            console.log("existing db -> "+dbName);
                        });
                        if(!_.include(dbs,db.name)) {
                            var msg = "no db "+db.name+"-> creating new one";
                            console.log(msg);
                            return r.pool
                                .run(r.dbCreate(db.name))
                                .then(function(res){
                                    return initTable();
                                });
                        }
                        else {
                            return initTable();
                        }
                    });
            }
        };
    }

Obsługę żądań o użytkownika (get, add, delete) też umieszczę w osobnym pliku *users.js*.

    //[users.js]
    "use strict";
    var parse = require("co-body");
    var db = require("./db/schema.js");

    module.exports = function(rethinkPool) {
        var r = rethinkPool;
        return {
            add : function* () {
                var newUser = yield parse.json(this);
                try {
                    var query = r.db(db.name)
                        .table(db.tables.users)
                        .insert(newUser, {returnChanges: true});
                    var changes = yield r.pool.run(query);
                    var obj = changes.changes[0].new_val;
                    this.body = obj;
                    this.set(“Location”, “/user/”+obj.id);
                    this.status = 201;
                } catch (e) {
                    this.body = e.message || http.STATUS_CODES[500];
                    this.statuses = 500;
                }
            },
            get : function* (id) {
                try {
                    var query = r.db(db.name)
                        .table(db.tables.users)
                        .get(id);
                    var user = yield r.pool.run(query);
                    if(user) {
                        this.body = user;
                        this.status = 200;
                    } else {
                        this.body = "No user with id: "+ id;
                        this.status = 404;
                    }
                } catch (e) {
                    this.status = 500;
                    this.body = e.message || http.STATUS_CODES[500];
                }
            },
            delete : function* (id) {
                try {
                    var query = r.db(db.name)
                        .table(db.tables.users)
                        .get(id)
                        .delete();
                    var operation = yield r.pool.run(query);
                    if(operation ) {
                        this.body = operation;
                        this.status = 200;
                    } else {
                        this.body = "No user with id: "+ id;
                        this.status = 404;
                    }
                } catch (e) {
                    this.status = 500;
                    this.body = e.message || http.STATUS_CODES[500];
                }
            }
        };
    };
Po dodaniu pliku konfiguracji *config.js*  nasz folder projektu wygląda następująco:

    ~/Projects/koa-lab/
    ▸ node_modules/
    ▾ server-app/
        ▾ db/
            initDb.js
            schema.js
        app.js
        config.js
        users.js
    .gitignore
    package.json


    // [config.js]
    module.exports = {
        rethinkDb: {
            host: 'localhost',
            port: "32769",
            authKey: null
        }
    };
Pozostaje kosmetyka w *app.js*.

    // [app.js]
    "use strict";

    var koa = require("koa");
    var route = require("koa-route");
    var app = null;
    
    var config = require("./config.js");
    var dbSchema = require("./db/schema.js");
    var moment = require("moment");
    config.rethinkDb.db = dbSchema.name;
    
    var r = require("ti-rethinkdb-pool")(config.rethinkDb);
    var users = require("./users.js")(r);
    
    function* requestTime(next) {
        var start = new Date();
        yield next;
        var time = new Date() - start;
        this.set("X-Response-Time",time+"ms");
    }
    function* logRequest(next) {
        var dateTimeFormat = "YYYY-MM-DD hh:mm:ss";
        var logMsg = "["
            + moment().format(dateTimeFormat)
            +"] " + this.method 
            +", url: " + this.url;
        console.log(logMsg);
        yield next;
    }

    function applicationStart() {
        app = koa();
    
        app.use(requestTime);
        app.use(logRequest);
     
        app.use(route.get("/user/:id", users.get));
        app.use(route.del("/user/:id", users.delete));
        app.use(route.post("/user", users.add));
    
        app.on("error", function(err, ctx) {
            console.log("server error", error, ctx);
        });
    
        app.listen(9000);
        console.log("server is listening on port 9000");
    }
    
    var initDb = require("./db/initDb.js")(r);
    initDb.initialize().then(applicationStart);

[Dalszy ciąg w trakcie tworzenia...]
