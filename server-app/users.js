"use strict";
var parse = require("co-body");
var db = require("./db/schema.js");

module.exports = function(rethinkPool) {
    var r = rethinkPool;
    return {
        add : function* () {
            var newUser = yield parse.json(this);
            try {
                var query = r.db(db.name)
                    .table(db.tables.users)
                    .insert(newUser, {returnChanges: true});
                var changes = yield r.pool.run(query);
                var obj = changes.changes[0].new_val;
                this.body = obj;
                this.set("Location", "/user/"+obj.id);
                this.status = 201;
            } catch (e) {
                this.throw(e.message || http.STATUS_CODES[500], 500);
            }
        },
        get : function* (id) {
            try {
                var query = r.db(db.name)
                    .table(db.tables.users)
                    .get(id);
                var user = yield r.pool.run(query);
                if(user) {
                    this.body = user;
                    this.status = 200;
                } else {
                    this.body = "No user with id: "+ id;
                    this.status = 404;
                }
            } catch (e) {
                this.status = 500;
                this.body = e.message || http.STATUS_CODES[500];
            }
        },
        listAll: function* () {
            try{
                var query = r.db(db.name)
                    .table(db.tables.users);
                var users = yield r.pool.run(query);
                this.body = users;
                this.status = 200;
            }catch(e){
                this.status = 500;
                this.body = e.message || http.STATUS_CODES[500];
            }
        },
        delete : function* (id) {
            try {
                var query = r.db(db.name)
                    .table(db.tables.users)
                    .get(id)
                    .delete();
                var operation = yield r.pool.run(query);
                if(operation ) {
                    this.body = operation;
                    this.status = 200;
                } else {
                    this.body = "No user with id: "+ id;
                    this.status = 404;
                }
            } catch (e) {
                this.status = 500;
                this.body = e.message || http.STATUS_CODES[500];
            }
        }
    };
};
