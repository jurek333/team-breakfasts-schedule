function initDb(rethinkPool) {
    var r = rethinkPool;
    var db = require("./schema.js");
    var _ = require("lodash");

    var initTable = function() {
        return r.pool.run(r.db(db.name).tableList()).then(function(tables){
            _.each(tables , function(tableName){
                console.log("existing table -> "+tableName);
            });
            _.each(db.tables, function(val, key){
                if(!_.include(tables, val)) {
                    r.pool
                        .run(r.db(db.name).tableCreate(val))
                        .then(function(){
                            console.log("table "+val+" created");
                        });
                }
            });
        });
    }
    return {
        initialize: function () {
            return r.pool.run(r.dbList()).then(function(dbs){
                _.each(dbs, function(dbName){
                    console.log("existing db -> "+dbName);
                });
                if(!_.include(dbs,db.name)) {
                    console.log("no db "+db.name+"-> creating new one");
                    return r.pool
                    .run(r.dbCreate(db.name))
                    .then(function(res){
                        return initTable();
                    });
                }
                else {
                    return initTable();
                }
            });
        }
    };
}
module.exports = initDb;
