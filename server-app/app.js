"use strict";

var koa = require("koa");
var route = require("koa-route");
var app = null;

var dbSchema = require("./db/schema.js");
var moment = require("moment");

var config = {
    rethinkDb: {}
};

function readConfiguration(){
    var dbHost = process.env.RDB_PORT_28015_TCP_ADDR || process.env.RDB_HOST;
    var dbPort = process.env.RDB_PORT_28015_TCP_PORT || 28015;
    var dbAuthKey = process.env.RDB_AUTHKEY;

    console.log("connecting: " + dbHost + ":" + dbPort);

    return {
        host: dbHost,
        port: dbPort,
        authKey: dbAuthKey,
        db: dbSchema.name
    };
}
config.rethinkDb = readConfiguration();
var r = require("ti-rethinkdb-pool")(config.rethinkDb);
var users = require("./users.js")(r);

function* requestTime(next) {
    var start = new Date();
    yield next;
    var time = new Date() - start;
    this.set("X-Response-Time",time+"ms");
}
function* logRequest(next) {
    var dateTimeFormat = "YYYY-MM-DD hh:mm:ss";
    var logMsg = "["
        + moment().format(dateTimeFormat)
        +"] " + this.method
        +", url: " + this.url;
    console.log(logMsg);
    yield next;
}

function applicationStart() {
    app = koa();

    app.use(requestTime);
    app.use(logRequest);

    app.use(route.get("/user/:id", users.get));
    app.use(route.del("/user/:id", users.delete));
    app.use(route.post("/user", users.add));
    app.use(route.get("/users", users.listAll));

    app.on("error", function(err, ctx) {
        console.log("server error", error, ctx);
    });

    app.listen(9000);
    console.log("server is listening on port 9000");
}

var initDb = require("./db/initDb.js")(r);
initDb.initialize().then(applicationStart);
